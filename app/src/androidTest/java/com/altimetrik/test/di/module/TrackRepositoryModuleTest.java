package com.altimetrik.test.di.module;

import com.altimetrik.test.data.remote.TrackApi;

import org.mockito.Mockito;

import java.time.Clock;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.junit.Assert.*;

@Module
public class TrackRepositoryModuleTest {

    @Provides
    @Singleton
    TrackApi getTrackService() {
        return Mockito.mock(TrackApi.class);
    }
}
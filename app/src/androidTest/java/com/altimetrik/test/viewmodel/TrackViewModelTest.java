package com.altimetrik.test.viewmodel;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.MockitoAnnotations;

import io.reactivex.Single;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TrackViewModelTest {

    private TrackViewModel viewModel;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        viewModel = new TrackViewModel();

    }


    @Test
    public void testApiFetchTrackSuccess() {
        // Mock API response
        viewModel.getTrackRepository();
    }

    @After
    public void tearDown() throws Exception {
        viewModel = null;
    }
}
package com.altimetrik.test;

import android.app.Application;
import android.content.Context;

public class AltimetrikApplication extends Application {

    /**
     * Instance of our application
     */
    private static AltimetrikApplication instance;

    /**
     *
     * @return Context of our application
     */
    public static Context getContext(){
        return instance;
    }

    @Override
    public void onCreate() {

        instance = this;

        super.onCreate();

    }

}

package com.altimetrik.test.base;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.altimetrik.test.data.local.SharedPreferenceManager;
import com.altimetrik.test.data.remote.TrackRepository;
import com.altimetrik.test.di.component.ApplicationComponent;
import com.altimetrik.test.di.component.DaggerApplicationComponent;
import com.altimetrik.test.di.module.ContextModule;

import javax.inject.Inject;

public abstract class DaggerBaseActivity extends AppCompatActivity {

    @Inject
    SharedPreferenceManager sharedPreferenceManager;

    /**
     * Repository of {@link com.altimetrik.test.model.Track} api call
     */
    @Inject
    TrackRepository trackRepository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ApplicationComponent component = DaggerApplicationComponent.builder()
                .contextModule(new ContextModule(this))
                .build();
        sharedPreferenceManager = component.initializeSharedPreferenceManager();
        trackRepository = component.getTrackRepository();

    }

    public SharedPreferenceManager getSharedPreferenceManager() {
        return sharedPreferenceManager;
    }

    public TrackRepository getTrackRepository() {
        return trackRepository;
    }

}

package com.altimetrik.test.di.component;

import com.altimetrik.test.data.local.SharedPreferenceManager;
import com.altimetrik.test.data.remote.TrackRepository;
import com.altimetrik.test.di.module.SharedPreferenceManagerModule;
import com.altimetrik.test.di.module.TrackRepositoryModule;

import javax.inject.Singleton;

import dagger.Component;


@Singleton
@Component(modules = {SharedPreferenceManagerModule.class, TrackRepositoryModule.class})
public interface ApplicationComponent {

    SharedPreferenceManager initializeSharedPreferenceManager();

    TrackRepository getTrackRepository();

}

package com.altimetrik.test.model;

import java.util.HashMap;

public enum AlbumSortingType {
    ALL(0),
    COLLECTION_NAME(1),
    TRACK_NAME(2),
    ARTIST_NAME(3),
    COLLECTION_PRICE(4);

    private int intValue;
    private static HashMap<Integer, AlbumSortingType> mappings;

    private static HashMap<Integer, AlbumSortingType> getMappings() {
        if (mappings == null) {
            synchronized (AlbumSortingType.class) {
                if (mappings == null) {
                    mappings = new HashMap<>();
                }
            }
        }
        return mappings;
    }

    private AlbumSortingType(int value) {
        intValue = value;
        getMappings().put(value, this);
    }

    public int getValue() {
        return intValue;
    }

    public static AlbumSortingType forValue(int value) {
        return getMappings().get(value);
    }

}

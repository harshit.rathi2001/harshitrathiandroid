package com.altimetrik.test.model;

import android.text.TextUtils;

import java.util.Comparator;

public class SortArtistName implements Comparator<Track> {

    @Override
    public int compare(Track track, Track t1) {
        if(TextUtils.isEmpty(track.getArtistName()) || TextUtils.isEmpty(t1.getArtistName())) return 1;

        return t1.getArtistName().compareTo(track.getArtistName());
    }
}

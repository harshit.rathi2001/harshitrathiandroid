package com.altimetrik.test.model;

import android.text.TextUtils;

import java.util.Comparator;

public class SortCollectionName implements Comparator<Track> {

    @Override
    public int compare(Track track, Track t1) {
        if(TextUtils.isEmpty(track.getCollectionName()) || TextUtils.isEmpty(t1.getCollectionName())) return 1;

        return t1.getCollectionName().compareTo(track.getCollectionName());
    }
}

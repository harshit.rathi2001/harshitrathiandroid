package com.altimetrik.test.model;

import java.util.Comparator;

public class SortCollectionPrice implements Comparator<Track> {

    @Override
    public int compare(Track track, Track t1) {
        if(track.getCollectionPrice() == null || t1.getCollectionPrice() == null) return 1;

        return t1.getCollectionPrice().compareTo(track.getCollectionPrice());
    }
}

package com.altimetrik.test.model;

import java.util.Comparator;

public class SortReleaseDate implements Comparator<Track> {

    @Override
    public int compare(Track track, Track t1) {
        return track.getReleaseDate().compareTo(t1.getReleaseDate());
    }
}

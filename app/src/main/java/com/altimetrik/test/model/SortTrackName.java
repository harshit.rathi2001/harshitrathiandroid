package com.altimetrik.test.model;

import android.text.TextUtils;

import java.util.Comparator;

public class SortTrackName implements Comparator<Track> {

    @Override
    public int compare(Track track, Track t1) {
        if(TextUtils.isEmpty(track.getTrackName()) || TextUtils.isEmpty(t1.getTrackName())) return 1;

        return t1.getTrackName().compareTo(track.getTrackName());
    }
}

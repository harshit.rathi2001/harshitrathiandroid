package com.altimetrik.test.view.ui;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.altimetrik.test.R;
import com.altimetrik.test.base.BaseActivity;
import com.altimetrik.test.databinding.ActivityMainBinding;
import com.altimetrik.test.model.AlbumSortingType;
import com.altimetrik.test.model.SortArtistName;
import com.altimetrik.test.model.SortCollectionName;
import com.altimetrik.test.model.SortCollectionPrice;
import com.altimetrik.test.model.SortReleaseDate;
import com.altimetrik.test.model.SortTrackName;
import com.altimetrik.test.model.Track;
import com.altimetrik.test.model.TrackResponse;
import com.altimetrik.test.utility.Constants;
import com.altimetrik.test.utility.Utility;
import com.altimetrik.test.view.adapter.TrackRecyclerViewAdapter;
import com.altimetrik.test.viewmodel.MainViewModel;
import com.altimetrik.test.viewmodel.TrackViewModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import butterknife.BindView;
import butterknife.OnEditorAction;
import segmented_control.widget.custom.android.com.segmentedcontrol.SegmentedControl;

public class MainActivity extends BaseActivity {

    /**
     * Data binding for activity
     */
    ActivityMainBinding activityMainBinding;

    /**
     * Activity UI components
     */
    @BindView(R.id.recyclerViewTrack)
    RecyclerView rvTrackList;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout srlLoading;

    @BindView(R.id.segmentedCategories)
    SegmentedControl smCategories;

    @BindView(R.id.editTextTerm)
    EditText etTerm;

    String countryCode = Constants.DEFAULT_COUNTRY_CODE;

    int mediaTypePosition = Constants.DEFAULT_MEDIA_POSITION;

    String term = Constants.DEFAULT_TERM;

    /**
     * {@link ArrayList} to hold the resulting track items from API call
     */
    ArrayList<Track> trackArrayList = new ArrayList<>();

    /**
     * {@link RecyclerView} adapter to bind the resulting data to the activity view
     */
    TrackRecyclerViewAdapter trackRecyclerViewAdapter;

    /**
     * {@link androidx.lifecycle.ViewModel} to hold {@link Track} data
     */
    TrackViewModel trackViewModel;

    /**
     * {@link androidx.lifecycle.ViewModel} to hold {@link MainActivity} configuration data
     */
    MainViewModel mainViewModel;

    /**
     * Saving the {@link MainActivity} as the last visited activity
     */
    @Override
    public void onResume() {
        getSharedPreferenceManager().setLastActivity(getClass().getName());
        super.onResume();
    }

    @Override
    protected int layoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Obtain binding object using the Data Binding library
        activityMainBinding = (ActivityMainBinding) getActivityMainBinding();

        setViewModel();

        setUI();

        callSearch();

    }

    /**
     * Method to get the view models to handle data for this UI
     */
    public void setViewModel(){

        trackViewModel = ViewModelProviders.of(this).get(TrackViewModel.class);

        mainViewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        activityMainBinding.setViewModel(mainViewModel);

    }

    /**
     * Method to set default values of the activity UI
     */
    public void setUI(){

        // Set term value and bind it to the search EditText widget
        mainViewModel.setSearchTerm(getSharedPreferenceManager().getTerm());

        // Set last search date and bind it to the date TextView widget
        mainViewModel.setLastSearch(Utility.formatDate(getSharedPreferenceManager().getLastSearchDate()));

        // Calling attemptSearch method when user swipes the track list to attempt calling Search API
        srlLoading.setOnRefreshListener(this::attemptSearch);

        observeCountryCodeValue();

        setupCategoryFilterListener();

        setupRecyclerView();
    }

    /**
     * Calling method to observe any changes on the country code value
     */
    private void observeCountryCodeValue(){


    }

    /**
     * Listening to filter category changes
     */
    private void setupCategoryFilterListener(){

        smCategories.addOnSegmentClickListener(segmentViewHolder -> {

            mediaTypePosition = segmentViewHolder.getAbsolutePosition();

            switch (AlbumSortingType.forValue(mediaTypePosition)) {
                case ALL:
                    Collections.sort(this.trackArrayList, new SortReleaseDate());
                    break;
                case COLLECTION_NAME:
                    Collections.sort(this.trackArrayList, new SortCollectionName());
                    break;
                case TRACK_NAME:
                    Collections.sort(this.trackArrayList, new SortTrackName());
                    break;
                case ARTIST_NAME:
                    Collections.sort(this.trackArrayList, new SortArtistName());
                    break;
                case COLLECTION_PRICE:
                    Collections.sort(this.trackArrayList, new SortCollectionPrice());
                    break;
            }

            trackRecyclerViewAdapter.notifyDataSetChanged();

        });

    }

    /**
     * Set up RecyclerView adapter and layout manager
     */
    private void setupRecyclerView() {
        if (trackRecyclerViewAdapter == null) {
            trackRecyclerViewAdapter = new TrackRecyclerViewAdapter(MainActivity.this, trackArrayList);
            rvTrackList.setLayoutManager(new LinearLayoutManager(this));
            rvTrackList.setAdapter(trackRecyclerViewAdapter);
            rvTrackList.setItemAnimator(new DefaultItemAnimator());
            rvTrackList.setNestedScrollingEnabled(true);
        } else {
            trackRecyclerViewAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Binded action for the search {@link EditText} widget
     * Call attemptSearch method after user hits the search button in the SoftKeyboard
     *
     * @param actionId The actionId returned by the listener. Only proceeds if value is EditorInfo.IME_ACTION_SEARCH
     * @return The result if the search action is handled
     */
    @OnEditorAction(R.id.editTextTerm)
    public boolean onEditorAction(int actionId){
        if (actionId == EditorInfo.IME_ACTION_SEARCH) {
            attemptSearch();
            return true;
        }
        return false;
    }

    /**
     * Evaluate if all requirements are completed for a valid Search API call
     * The parameter key 'term' is required  by the iTunes Search api so check first if value is not empty before proceeding
     */
    private void attemptSearch(){
        Utility.hideKeyboard(MainActivity.this, etTerm);
        term = etTerm.getText().toString();

        if(TextUtils.isEmpty(term)) {
            Utility.setEditTextError(etTerm, getString(R.string.required));
            return;
        }

        saveData();

        mainViewModel.setLastSearch(Utility.formatDate(getSharedPreferenceManager().getLastSearchDate()));

        callSearch();

    }

    /**
     * Method to save the data on {@link android.content.SharedPreferences}
     */
    public void saveData(){

        // Save new value of search term locally
        getSharedPreferenceManager().setTerm(term);

        // Save new value of last search date locally
        getSharedPreferenceManager().setLastSearchDate(Utility.getCurrentDateTime());
    }

    /**
     * Method to call the searchTrack function from network repository. This function will call the iTunes Search API
     */
    public void callSearch(){
        trackArrayList.clear();
        trackViewModel.searchTrack(getTrackRepository(), getSharedPreferenceManager().getTerm());
        observeTrackDataResponse();
    }

    /**
     * Observe API call response and update {@link RecyclerView} list for data changes
     */
    private void observeTrackDataResponse(){
        mainViewModel.setRefreshing(true);
        trackViewModel.getTrackRepository().observe(this, trackResponseResult -> {
            if(trackResponseResult.getResponseBody().isSuccessful()){
                List<Track> tracks = ((TrackResponse) trackResponseResult.getResponse()).getResults();
                mainViewModel.setIsResultEmpty(tracks.size() <= 0);
                Set<Track> s = new HashSet<>(tracks);
                trackArrayList.addAll(s);
                Collections.sort(trackArrayList, new SortReleaseDate());
            }
            else{
                // Show a message feedback to user when api response is not successful due to network error
                if(!Utility.isNetworkAvailable(MainActivity.this)) {
                    Toast.makeText(MainActivity.this, getString(R.string.no_network_available), Toast.LENGTH_SHORT).show();
                    trackArrayList.clear();
                }
            }

            trackRecyclerViewAdapter.notifyDataSetChanged();
            mainViewModel.setRefreshing(false);
        });
    }
}
